const Image = require('../models/imagesModel');
const contBase = require('./baseController');

exports.delete = async (req, res, next)=>{
    try{
        await Image.findByIdAndUpdate(req.image.id,{
            active: false
        });

        res.status(204).json({
            status:'success',
            data: null
        });
    }
    catch (ex){
        next(ex);
    }
};

exports.getAll = contBase.getAll(Image);
exports.getOne = contBase.getOne(Image);
exports.updateImage = contBase.updateOne(Image);
exports.deleteImage = contBase.deleteOne(Image);