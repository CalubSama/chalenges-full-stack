const express = require('express');
const { port } = require('./config');
const app = express();
console.log("port>>>>>>>>>",port);
require('./database');

app.use(require('./routes/imagesRoutes'))
app.listen(port, () => console.log('Server is running'));