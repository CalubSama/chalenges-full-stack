const dotenv = require('dotenv');
dotenv.config();

module.exports = {
    //endpoint: process.env.API_URL,
    database_url: process.env.DATABASE_URL,
    port: process.env.PORT
}