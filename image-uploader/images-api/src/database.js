const { database_url } = require('./config');
const mongoose  = require('mongoose');

console.log(`db route ---->,  ${database_url}`);

mongoose.connect(database_url)
.then(db => console.log('Db is connected to', db.connection.host))
.catch(err => console.error('error trying to connected', err));