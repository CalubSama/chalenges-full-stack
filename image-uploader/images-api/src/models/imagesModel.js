const mongoose = require('mongoose');

const imageSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please fill a title for the image'],
  },
  category: {
    type: String,
    required: [false, 'Please fill a category for the image'],
  },
});

const Image = mongoose.model('Image', imageSchema);
module.exports = Image;